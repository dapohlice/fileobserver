package code.pohli.fileObserver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import code.pohli.fileObserver.queue.WatchingQueue;
import code.pohli.fileObserver.util.ChangeProcessor;
import code.pohli.fileObserver.util.FileManager;
import code.pohli.fileObserver.util.Walker;
import code.pohli.fileObserver.watcher.WatchServiceManager;
import code.pohli.utils.console.Console;

/***
 * 
 * @author alex
 * Observes Files and Directories in a Directory and detectes Creating, Modifying and Deleteing Files and Directory
 */
public class FileObserver {

	private Path path;
	
	private WatchingQueue queue;
	private ChangeProcessor changeProcessor;
	
	private WatchServiceManager watchServiceManager;
	private FileManager fileManager;
	
	Thread t_changeProcessor;
	
	
	public FileObserver(IFileChanger changer,Path path)
	{
		
		this.path = path;
		
		this.queue = new WatchingQueue();
		
		this.watchServiceManager = new WatchServiceManager(queue);
		
		this.fileManager = new FileManager(watchServiceManager,changer);
		
		this.changeProcessor = new ChangeProcessor(queue, fileManager);
			
		
	}
	
	public boolean start() {
		
		Console.debug("start FileObserver for "+path.toAbsolutePath().toString());
		
		Walker fileWalker = new Walker(fileManager);
		
		try {
			Files.walkFileTree(this.path, fileWalker);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		
		t_changeProcessor = new Thread(this.changeProcessor);
		t_changeProcessor.setName("ChangeProcessor for "+path.toAbsolutePath().toString());;
		t_changeProcessor.start();
		
		
		return true;
	}
	
	public void stop() {
		Console.debug("stop FileOberserver");
		
		this.watchServiceManager.stopRunning();
		this.changeProcessor.stopRunning();
		
	}
	
	
}
