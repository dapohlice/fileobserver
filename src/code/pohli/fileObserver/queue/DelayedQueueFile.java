package code.pohli.fileObserver.queue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayedQueueFile extends QueueFile implements Delayed, IQueueFile{

	public static long delay = 300;
	
	private volatile long time;

	
	
	public DelayedQueueFile(Path path,QueueFileStatus status)
	{
		this(path,status,false);
	}
	
	public DelayedQueueFile(Path path, QueueFileStatus status, boolean created) {
		super(path,status,created);
		setTime();	
	}
	
	private void setTime(){
		this.time = System.currentTimeMillis()+DelayedQueueFile.delay;
	}
	
	

	public void delayToZero(){
		this.time = System.currentTimeMillis()-1;
	}
	
	
	@Override
	public void setModify(){
		super.setModify();
		setTime();
	}
	
	@Override
	public void setDelete(){
		super.setDelete();
		setTime();
	}
	
	@Override
	public void setCreate(){
		super.setCreate();
		setTime();
	}

	@Override
	public long getDelay(TimeUnit unit){
		long diff = this.time - System.currentTimeMillis();
		return unit.convert(diff, TimeUnit.MILLISECONDS);
	}

	@Override
	public int compareTo(Delayed obj) {
		if(obj instanceof DelayedQueueFile)
		{
			DelayedQueueFile other = (DelayedQueueFile) obj;
			if(this.time > other.time)
			{
				return 1;
			}else{
				if(this.time == other.time)
				{
					return 0;
				}
			}
		}
		
		return -1;
	}

	@Override
	public String toString() {
		return "DelayedQueueFile " +this.getDelay(TimeUnit.MILLISECONDS)+ " " +getFullPath() +" ( "+ getStatus() + " , " + isCreate() + ")";
	}
	
	
	
}
