package code.pohli.fileObserver.queue;

public enum QueueFileStatus {
	Create,
	Modify,
	Delete
}
