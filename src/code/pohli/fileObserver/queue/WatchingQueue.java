package code.pohli.fileObserver.queue;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.DelayQueue;

import code.pohli.utils.console.Console;

public class WatchingQueue {

	private DelayQueue<DelayedQueueFile> queue;
	private Map<String,DelayedQueueFile> map;
	
	
	public WatchingQueue() {
		this.queue = new DelayQueue<DelayedQueueFile>();
		this.map = new HashMap<String,DelayedQueueFile>();
	}
	
	public void setFileToDelete(Path path)
	{
		DelayedQueueFile file = this.map.get(path.toString());
		if(file == null)
		{
			file = new DelayedQueueFile(path, QueueFileStatus.Delete);
			this.map.put(path.toString(), file);
		}else{
			file.setDelete();
		}
		
		if(!this.queue.contains(file)) {
			this.queue.add(file);
		}
		
		
		Console.debug("WQueue to del "+file.toString());
	}
	
	
	public void setFileToModify(Path path)
	{
		
		
		DelayedQueueFile file = this.map.get(path.toString());
		if(file == null)
		{
			file = new DelayedQueueFile(path, QueueFileStatus.Modify);
			this.map.put(path.toString(), file);
		}else{
			file.setModify();
		}
		
		if(!this.queue.contains(file)) {
			this.queue.add(file);
		}
		
		Console.debug("WQueue to parse "+file.toString());
	}
	
	
	public void setFileToCreate(Path path)
	{
		DelayedQueueFile file;
		
		if(this.map.containsKey(path.toString()))
		{
			 file = this.map.get(path.toString());
			 file.setCreate();
		}else {
			file = new DelayedQueueFile(path,QueueFileStatus.Create,true);
			this.map.put(path.toString(), file);
			
			this.queue.add(file);
		}
		

		
		Console.debug("WQueue to create "+file.toString());
	}
	
	
	public IQueueFile getNext(){
		return this.queue.poll();
	}
	
	public void setDelayToZero(){
		Iterator<DelayedQueueFile> it = this.queue.iterator();
		
		while(it.hasNext())
		{
			DelayedQueueFile qf = it.next();
			qf.delayToZero();
		}
		
	}
	
	
	
	public boolean isEmpty(){
		return this.queue.isEmpty();
	}

}
