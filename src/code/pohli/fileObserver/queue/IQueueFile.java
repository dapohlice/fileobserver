package code.pohli.fileObserver.queue;

import java.nio.file.Path;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public interface IQueueFile {

	void setModify();

	void setDelete();

	void setCreate();

	boolean isCreate();

	boolean isModify();

	boolean isDelete();

	QueueFileStatus getStatus();

	boolean isDirectory();

	Path getPath();

	String getFullPath();


	String toString();

}