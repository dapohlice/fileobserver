package code.pohli.fileObserver.queue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class QueueFile implements IQueueFile{

	private volatile QueueFileStatus status;
	private boolean created;
	private Path path;
	
	
	public QueueFile(Path path,QueueFileStatus status)
	{
		this(path,status,false);
	}
	
	public QueueFile(Path path, QueueFileStatus status, boolean created) {
		this.path = path;
		this.status = status;
		this.created = created;			
	}

	
	@Override
	public void setModify(){
		this.status = QueueFileStatus.Modify;
	}
	
	@Override
	public void setDelete(){
		this.status = QueueFileStatus.Delete;
	}
	
	@Override
	public void setCreate(){
		this.status = QueueFileStatus.Create;
	}
	
	@Override
	public boolean isCreate(){
		return this.status == QueueFileStatus.Create;
	}
	
	@Override
	public boolean isModify(){
		return this.status == QueueFileStatus.Modify;
	}
	
	@Override
	public boolean isDelete(){
		if(this.status == QueueFileStatus.Delete)
		{
			//return !this.created;
			return true;
		}
		return false;
	}
	
	
	@Override
	public QueueFileStatus getStatus() {
		return this.status;
	}
	
	@Override
	public boolean isDirectory() {
		return Files.isDirectory(getPath());
	}
	
	
	@Override
	public Path getPath(){
		return this.path;
	}
	
	@Override
	public String getFullPath(){
		return this.path.toAbsolutePath().toString();
	}

	@Override
	public String toString() {
		return "QueueFile " +path.toAbsolutePath().toString() +" ( "+ status + " , " + created + ")";
	}
	
	
	
}
