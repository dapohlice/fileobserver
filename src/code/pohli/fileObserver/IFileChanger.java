package code.pohli.fileObserver;

import java.nio.file.Path;

import code.pohli.fileObserver.queue.IQueueFile;

public interface IFileChanger {
	
	void deleteFile(Path file);
	
	void createFile(Path file);
	
	void modifyFile(Path file);
	
	void deleteDir(Path dir);
	
	void createDir(Path dir);
	
	void modifyDir(Path dir);
	
}
