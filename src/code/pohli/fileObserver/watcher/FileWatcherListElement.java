package code.pohli.fileObserver.watcher;

import java.util.ArrayList;

public class FileWatcherListElement{
	private ArrayList<FileWatcher> list;
	private FileWatcher mainWatcher;
	
	public FileWatcherListElement(){
		list = new ArrayList<FileWatcher>();
	}
	
	public void addSubWatcher(FileWatcher watcher)
	{
		list.add(watcher);
	}
	
	public void removeSubWatcher(FileWatcher watcher)
	{
		list.remove(watcher);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<FileWatcher> getAllWatcher(){
		ArrayList<FileWatcher> clonedList = (ArrayList<FileWatcher>)list.clone();
		clonedList.add(mainWatcher);
		return clonedList;		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<FileWatcher> getAllSubWatcher(){
		return (ArrayList<FileWatcher>)list.clone();
				
	}
	
	public void addMainWatcher(FileWatcher watcher)
	{
		mainWatcher = watcher;
	}
	
	public FileWatcher getMainWatcher() {
		return mainWatcher;
	}
	
	
	
	
	
}
