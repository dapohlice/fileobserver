package code.pohli.fileObserver.watcher;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;

import code.pohli.fileObserver.util.PathMap;


public class FileWatcherList {

	private PathMap<FileWatcherListElement> mapper;
		
	
	public FileWatcherList()
	{
		mapper = new PathMap<FileWatcherListElement>();
	}
	
	
	public boolean removeWatcher(Path path)
	{
		FileWatcherListElement element = mapper.get(path);
		if(element == null)
			return false;
		
		for(FileWatcher watcher: element.getAllSubWatcher())
		{
			mapper.remove(watcher.getPath());
		}
		
		return mapper.remove(path);
	}
	
	
	public FileWatcher get(Path path)
	{
		 FileWatcherListElement element = mapper.get(path);
		 
		 if(element == null)
			 return null;
		 
		 return element.getMainWatcher();
	}
	
	
	
	public void put(FileWatcher watcher) {
		insertMainWatcher(watcher);
		
		Path watcherPath = watcher.getPath();
		
		Path parent = watcherPath.getParent();
		while(parent != null)
		{
			insertWatcher(parent,watcher);
			parent = parent.getParent();
		}
	}
	
	private void insertMainWatcher(FileWatcher watcher)
	{
		FileWatcherListElement element = mapper.get(watcher.getPath());
		
		if(element == null) {
			element = new FileWatcherListElement();
			mapper.put(watcher.getPath(), element);
		}
		element.addMainWatcher(watcher);
		
	}
	
	
	private void insertWatcher(Path path, FileWatcher watcher)
	{
		FileWatcherListElement element = mapper.get(path);
		
		if(element == null)
		{
			element = new FileWatcherListElement();
			mapper.put(path, element);
		}
		
		element.addSubWatcher(watcher);
	}
	
	
	public ArrayList<FileWatcher> getAllWatcherByPath(Path path)
	{
		FileWatcherListElement element =  mapper.get(path);
		
		if(element == null)
			return new ArrayList<FileWatcher>();
		
		return element.getAllWatcher();
	}
	
	
	
	public Collection<FileWatcher> getAllWatcher() {
		ArrayList<FileWatcher> result = new ArrayList<FileWatcher>(mapper.size());
		
		for(FileWatcherListElement element : mapper.values()) {
			result.add(element.getMainWatcher());
		}
		
		return result;
	}
	

	
	
	
	
	
}
