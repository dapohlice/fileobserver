package code.pohli.fileObserver.watcher;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;

import code.pohli.fileObserver.queue.WatchingQueue;
import code.pohli.utils.console.Console;

public class WatchServiceManager {

	private FileWatcherList watcherList;
	private WatchingQueue queue;
	
	
	public WatchServiceManager(WatchingQueue queue) {
		this.watcherList = new FileWatcherList();
		this.queue = queue;
	}
	
	public void stopRunning(){
		Console.debug("stopping Watching Walker");
		for(FileWatcher fw : this.watcherList.getAllWatcher())
		{
			Console.debug("Stop FileWatcher");
			fw.stopRunning();
		}
		
		Console.debug("Watching Walker stopped");
	}
	
	
	public boolean registerService(Path dir)
	{
		try {
			WatchService watchService = FileSystems.getDefault().newWatchService();
			
			WatchKey key = dir.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY,StandardWatchEventKinds.ENTRY_DELETE,StandardWatchEventKinds.ENTRY_CREATE);
			
			FileWatcher fw = new FileWatcher(watchService, this.queue, dir);
			watcherList.put(fw);
			
			Thread tfw = new Thread(fw);
			tfw.setName("Watcher: "+dir.toString());
			
			tfw.start();
			
			Console.debug("Registered WatchService: "+dir.toString());
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Console.error("Watch Service Manager failed register service",e);
		}
		return true;
	}
	
	public boolean isPathRegistered(Path dir)
	{
		FileWatcher fw = watcherList.get(dir);
		boolean result = fw != null; 
		
		return result;
	}
	
	public void unregisterService(Path dir)
	{
		Console.debug("Stop Service for "+dir.toString());
		
		stopAllWatcherByPath(dir);
				
		watcherList.removeWatcher(dir);
		
		Console.debug("Stoped Service for "+dir.toString());
		
	}
	
	public void stopAllWatcherByPath(Path path)
	{
		ArrayList<FileWatcher> list = watcherList.getAllWatcherByPath(path);
		
		for(FileWatcher watcher:list)
		{
			watcher.stopRunning();
		}
	}
}
