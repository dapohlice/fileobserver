package code.pohli.fileObserver.watcher;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import code.pohli.fileObserver.queue.WatchingQueue;
import code.pohli.utils.console.Console;

public class FileWatcher implements Runnable{

	public static int timeout = 10;
	
	
	private volatile boolean running;
	private volatile boolean stopped;
	
	private WatchService service;
	private WatchingQueue queue;
	
	private Path path;
	
	
	
	
	public FileWatcher(WatchService service,WatchingQueue queue, Path path) {
		
		Console.debug("New FileWatcher "+path.toString());
		this.running = true;
		this.service = service;
		this.queue = queue;
		this.path = path;
		this.stopped = false;
	}
	
	public void stopRunning(){
		Console.debug("Stopping FileWatcher");
		
		this.running = false;
		
		
		while(!this.stopped)
		{
			Thread.yield();
		}
		
		boolean serviceRunning = true;
		
		do {
			try {
				this.service.close();
				serviceRunning = false;
			}catch(Exception e)
			{
				Console.debug("Watch Service cant be stopped, retry ...");
			}
		}while(serviceRunning);
		
		Console.debug("FileWatcher stopped");
	}
	

	@Override
	public void run() {
		
		Console.debug("Start FileWatcher for "+this.path.toString());
		
		while(this.running)
		{
			try{

				WatchKey key = this.service.poll();
				
				if(key != null)
				{
					for(WatchEvent<?> event : key.pollEvents()){
						WatchEvent.Kind<?> kind = event.kind();
						
						if(kind == StandardWatchEventKinds.OVERFLOW)
						{
							System.out.println("OVERFLOW");
							continue;
						}
						
						WatchEvent<Path> ev = (WatchEvent<Path>)event;
						Path filename = path.resolve(ev.context());
						
						Console.debug("File "+ filename.toAbsolutePath().toString() +" "+ kind.toString());
						
						if(!Files.isHidden(filename))
						{
							if(kind == StandardWatchEventKinds.ENTRY_DELETE)
							{
								queue.setFileToDelete(filename);
								
							
							}else{
								if(kind == StandardWatchEventKinds.ENTRY_MODIFY){
									
									queue.setFileToModify(filename);
								
								}else{
									if(kind == StandardWatchEventKinds.ENTRY_CREATE)
									{
										queue.setFileToCreate(filename);
									}
								}
							}
							
							
						}else{
							Console.debug(filename+" is hidden");
						}
						
						boolean valid = key.reset();
					    if (!valid) {
					        break;
					    }
					}
				}
				
				try{
					Thread.sleep(100);
				}catch(InterruptedException e)
				{
					this.stopRunning();
					Console.warn("Interrupt FileWatcher");
				}
			}catch(Exception e)
			{
				System.out.println("No Dir");
				e.printStackTrace();
				this.running = false;
			}
			
				
		}

		
		
		Console.log("FileWatcher for "+ path.toAbsolutePath().toString() +" end");
		this.stopped = true;
	}
	
	public Path getPath() {
		return this.path;
	}

}
