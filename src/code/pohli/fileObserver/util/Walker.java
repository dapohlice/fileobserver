package code.pohli.fileObserver.util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class Walker extends SimpleFileVisitor<Path> {

	private FileManager manager;
	
	private boolean hidden;
	
	public Walker(	
			FileManager fileManager
	) {
		super();
		
		this.manager = fileManager;
		
		this.hidden = false;
	}
	
	
	public void enableHiddeFiles() {
		this.hidden = true;
	}
	
	
	
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		
		this.manager.registerDirectory(dir);
		
		FileVisitResult result = super.postVisitDirectory(dir, exc);
		return result; 
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		if(Files.isHidden(dir))
		{
			return FileVisitResult.SKIP_SUBTREE;
		}
		FileVisitResult result = super.preVisitDirectory(dir, attrs);
		return result;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		if(this.hidden || !Files.isHidden(file))
		{
			manager.registerFile(file);
		}
		FileVisitResult result = super.visitFile(file, attrs);
		return result;
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		// TODO Auto-generated method stub
		FileVisitResult result =  super.visitFileFailed(file, exc);
		return result;
	}

}
