package code.pohli.fileObserver.util;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import code.pohli.fileObserver.IFileChanger;
import code.pohli.fileObserver.watcher.WatchServiceManager;
import code.pohli.utils.console.Console;

public class FileManager {

	private WatchServiceManager serviceManager;
	
	private TreeSet<Path> files;
	private TreeSet<Path> dirs;
	private DirectoryElementsMap dirFileMap;
	private DirectoryElementsMap dirDirMap;
	private IFileChanger fileChanger;
	
	public FileManager(WatchServiceManager watchServiceManager, IFileChanger fileChanger)
	{
		this.serviceManager = watchServiceManager;
		this.fileChanger = fileChanger;
		
		this.files = new TreeSet<Path>();
		this.dirs = new TreeSet<Path>();
		this.dirFileMap = new DirectoryElementsMap();
		this.dirDirMap = new DirectoryElementsMap();
		
	}
	
	public void registerFile(Path file) {
		Console.debug("Register File "+file.toString());
		
		fileChanger.createFile(file);
		
		files.add(file);
		dirFileMap.addElement(file);
	}

	public void fileModified(Path file)
	{
		Console.debug("File Modified"+file.toString());
		fileChanger.modifyFile(file);
	}
	
	public void unregisterFile(Path file) {
		Console.debug("Unregister File "+file.toString());
		
		fileChanger.deleteFile(file);
		
		files.remove(file);
		dirFileMap.removeElement(file);
		
		
	}

	public void registerDirectory(Path dir) {
		fileChanger.createDir(dir);
		dirs.add(dir);
		dirDirMap.addElement(dir);
		this.serviceManager.registerService(dir);

	}

	public void directoryModified(Path dir)
	{
		fileChanger.modifyDir(dir);
	}
	
	public void unregisterDirectory(Path dir) {
		
		deleteDirectory(dir);
		
		this.serviceManager.unregisterService(dir);
		dirs.remove(dir);
	}
	
	private void deleteDirectory(Path dir)
	{
		for(Path directory : dirDirMap.getElements(dir))
		{
			deleteDirectory(directory);
		}
		
		for(Path file : dirFileMap.getElements(dir))
		{
			fileChanger.deleteFile(file);
		}
		
		fileChanger.deleteDir(dir);
		
	}
	
	

	public boolean isDirectory(Path dir) {
		return dirs.contains(dir);
	}
	
	public boolean isFile(Path file) {
		return files.contains(file);
	}

	public boolean isKnown(Path path) {
		boolean isDir = isDirectory(path);
		boolean isFile = isFile(path);
		return isDir | isFile;
	}



}
