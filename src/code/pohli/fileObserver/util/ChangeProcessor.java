package code.pohli.fileObserver.util;

import java.nio.file.Files;

import code.pohli.fileObserver.IFileChanger;
import code.pohli.fileObserver.queue.IQueueFile;
import code.pohli.fileObserver.queue.WatchingQueue;
import code.pohli.fileObserver.watcher.WatchServiceManager;
import code.pohli.utils.console.Console;

public class ChangeProcessor implements Runnable{
		
	private WatchingQueue queue;
	
	private FileManager fileManager;
	
	private boolean running;
	private boolean stopped;
	
	public ChangeProcessor(
				WatchingQueue queue,
				FileManager fileManger
		
				)
	{
		
		this.queue = queue;
		
		this.fileManager = fileManger;
		
		this.running = true;
		this.stopped = false;
	}

	public void stopRunning(){
		
		
		this.queue.setDelayToZero();
		
		while(!this.queue.isEmpty())
		{
			Thread.yield();
		}
		
		this.running = false;
		
		while(!this.stopped)
		{
			Thread.yield();
		}
		
		Console.debug("Stop running ChangeProcessor executed");
	}
	
	
	@Override
	public void run() {
		
		while(this.running)
		{
			IQueueFile file = this.queue.getNext();
			if(file != null)
			{
				
				switch(file.getStatus()) {
					
				case Create:
						
						if(Files.isDirectory(file.getPath())){
							
							Console.debug("Directory Created: "+file.getFullPath());
							
							fileManager.registerDirectory(file.getPath());
						}else {
							Console.debug("File Created: "+file.getFullPath());
							
							fileManager.registerFile(file.getPath());
						}
					
					break;
					
				case Modify:
					
						if(Files.isDirectory(file.getPath()))
						{
							
							Console.debug("Directory Created: "+file.getFullPath());
							fileManager.directoryModified(file.getPath());
			
						}else {
							
							Console.debug("File Created: "+file.getFullPath());
							fileManager.fileModified(file.getPath());

						}
						
					break;
					
				case Delete:
					
						if(fileManager.isDirectory(file.getPath())) {
							Console.debug("Directory Deleted: "+file.getFullPath());
							
							fileManager.unregisterDirectory(file.getPath());
						}
					
						if(fileManager.isFile(file.getPath()))
						{
							Console.debug("File Deleted: "+file.getFullPath());
							
							fileManager.unregisterFile(file.getPath());
						}
						
					break;
				}

			}
			
			
			try{
				Thread.sleep(100);
			}catch(InterruptedException e)
			{
				this.stopRunning();
				Console.warn("Interupt Changed Processor");
			}
		}
		
		
		
		Console.log("ChangeProcessor End");
		this.stopped = true;
	}


}
