package code.pohli.fileObserver.util;

import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;

public class PathMap<T>{
	
	private HashMap<String,T> mapper;
	
	public PathMap(){
		mapper = new HashMap<String,T>();
	}
	
	
	public boolean remove(Path path)
	{
		return null != mapper.remove(getKey(path));
	}
	
	
	public void put(Path path, T element)
	{
		mapper.put(getKey(path),element);
	}
	

	public T get(Path path)
	{
		return mapper.get(getKey(path));
	}
	
	public String getKey(Path path)
	{
		return path.toAbsolutePath().toString();
	}
	
	public int size() {
		return mapper.size();
	}
	
	public Collection<T> values(){
		return mapper.values();
	}
	
}
