package code.pohli.fileObserver.util;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DirectoryElementsMap  {
	
	private PathMap<List<Path>> map;
	
	public DirectoryElementsMap() {
		map = new PathMap<List<Path>>();
	}
	
	
	public void addElement(Path path) {
		path = path.toAbsolutePath();
		List<Path> list = getPathList(path);
		
		if(list == null)
		{
			list = new ArrayList<Path>();
			map.put(path.getParent(), list);
		}
		
		list.add(path);
	}
	
	public void removeElement(Path path)
	{
		path = path.toAbsolutePath();
		List<Path> list = getPathList(path);
		
		if(list == null)
			return;
		
		for(Path p: list)
		{
			if(p.equals(path))
			{
				list.remove(p);
				return;
			}
		}
		
	}
	
	private List<Path> getPathList(Path path)
	{
		path = path.toAbsolutePath();
		Path parent = path.getParent();
		return map.get(parent);
	}
	
	public List<Path> getElements(Path path)
	{
		List<Path> result = map.get(path.toAbsolutePath());
		if(result != null)
			return result;
		
		return new ArrayList<Path>();
	}
	
	
	
}
